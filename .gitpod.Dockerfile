FROM gitpod/workspace-full:latest

RUN sudo apt update && sudo apt upgrade -y
RUN npm install -g learnpack && learnpack plugins:install learnpack-python

USER trainee
