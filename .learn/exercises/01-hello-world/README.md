# 00 - Hello World with Flask

It's recommended to use the latest version of Python 3. Flask itselfs supports Python 3.5 and newer, Python 2.7, and PyPy.

To check the installed version of Python use the following command:
`python --version` or `python3 --version`

Dependencies are automatically installed when FLask will be installed. So the next step is to install Flask.
`pip install Flask`
  
After you successfully installed Python, the next step is to deploy your first Flask Application. Let's start with an Basic Application, which looks like this.

```python
from flask import Flask

app = Flask(__name__)

@app.route('/')
def hello_world():
	return 'Hello, World!'
```

First, lets talk about the code.

1. First we imported the Flask class. An instance of this class will be our WSGI application.
2. Next we create an instance of this class.
3. We then use the route() decorator to tell Flask waht URL should trigger our function.
4. The function is given a name which is also used to generate URLs for that particular function, and returns the message we want to display in the user’s browser.

As you understand what we did we can safe the app as `app.py` for example. To run the application you can use either the `flask` command or the python's `-m` switch. But first you need to tell your terminal the application to work with by exporting the `FLASK_APP` environment variable:

```bash
$ export FLASK_APP=hello.py
$ flask run
	* Running on http://127.0.0.1:5000/
```